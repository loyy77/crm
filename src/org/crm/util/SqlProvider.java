package org.crm.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.hibernate.validator.util.GetClassLoader;

public class SqlProvider {
	

	public static String getSql(String className,String key){
		
		InputStream ins=GetClassLoader.class.getResourceAsStream(className+".properties");
	
		Properties p=new Properties();
		try {
			p.load(ins);
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return p.getProperty(key);
	}
}
