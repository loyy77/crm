/**
 * 创建数据表和数据表内容
 * @author LiShixi
 * @version 1.0
 */


import groovy.sql.Sql

import java.io.File
import java.util.Properties
import org.crm.entity.Chance

class SetupDatabase {
	
	def static fileLocation="WebContent/WEB-INF/spring/db.properties"
	def static db=["driverClassName":dbInfo("driverClassName"),"url":"jdbc:derby:c:/tmp/db/crm;create=true"]
	def static sql=null;

	//初始化数据库
	static main(args) {
		/*
		new SetupDatabase().initSchame()
		new SetupDatabase().initData()
		new SetupDatabase().findUsers()
		*/
		new SetupDatabase().derbyPage()
	}
	
	def derbyPage(){
		Sql sql=getConn()
		
		List list=	sql.rows("select * from (select row_number() over() as rownum,chance.* from chance where state!=1) as tmp where rownum<=5 and rownum>=1")
			for(int i =0;i<list.size();i++){
				println list.get(i)
			}
			
			print "hjh"
		
	}
	
	
	
	//到配置文件拿数据库连接信息
	def static dbInfo(def key){
		InputStream ins = new BufferedInputStream(new FileInputStream(fileLocation));
		Properties p = new Properties();
		p.load(ins);	
		p.getProperty(key)
	}
	//	查找所有用户
	def findUsers(){

		Sql sql=getConn()

		sql.eachRow("select * from users") { println it }
	}
	
	def  getConn(){
		sql=Sql.newInstance(db.url,db.driverClassName)

	}
	//导入数据
	def initData(){
		Sql sql=getConn();
		def sqlText=new File("crm-derby-data.sql").getText()
		String[] sqlStr=sqlText.split(";");

		for(i in sqlStr ){
			if(i.trim().equals(""))continue;
			try{
				def rst=sql.executeInsert(i)
				if(rst==null){
				
					}else{
					print rst!=1?"成功":"失败";
					
				}
			}catch(java.lang.NullPointerException ex){
				
			}
		}
		println "";
	}
	//	初始化表结构
	def initSchame(){

		Sql sql=getConn();
		def sqlText=new File("crm-derby-schame.sql").getText();
		String[] sql2=sqlText.split(";")
		println "开始创建数据表_>>>>>"
		for(i in sql2){
			if(i.trim().equals(""))continue;
			try{
				println i
				println sql.execute(i)==false?"成功":"失败";
				
			}catch(java.sql.SQLSyntaxErrorException ex){

			}catch(java.sql.SQLException e){
			}
		}
		println "<<<<<_结束创建数据表"
	}
	
	def closeDb(){
		
	}
	

}
